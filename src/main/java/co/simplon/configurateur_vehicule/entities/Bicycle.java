package co.simplon.configurateur_vehicule.entities;

public class Bicycle {
    private int id_bicycle;
    private String bicycleType;
    private int numberOfTrail;
    private boolean isMarked;

    public Bicycle(int id_bicycle, String bicycleType, int numberOfTrail, boolean isMarked) {
        this.id_bicycle = id_bicycle;
        this.bicycleType = bicycleType;
        this.numberOfTrail = numberOfTrail;
        this.isMarked = isMarked;
    }

    public Bicycle() {
    }

    public int getId_bicycle() {
        return id_bicycle;
    }

    public void setId_bicycle(int id_bicycle) {
        this.id_bicycle = id_bicycle;
    }

    public String getBicycleType() {
        return bicycleType;
    }

    public void setBicycleType(String bicycleType) {
        this.bicycleType = bicycleType;
    }

    public int getNumberOfTrail() {
        return numberOfTrail;
    }

    public void setNumberOfTrail(int numberOfTrail) {
        this.numberOfTrail = numberOfTrail;
    }

    public boolean isMarked() {
        return isMarked;
    }

    public void setMarked(boolean isMarked) {
        this.isMarked = isMarked;
    }
}
