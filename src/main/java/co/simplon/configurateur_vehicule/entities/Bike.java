package co.simplon.configurateur_vehicule.entities;

public class Bike {
    private int id_bike;
    private String bikeType;
    private int engine;
    private int horsePower;
    
    public Bike(int id_bike, String bikeType, int engine, int horsePower) {
        this.id_bike = id_bike;
        this.bikeType = bikeType;
        this.engine = engine;
        this.horsePower = horsePower;
    }
    
    public Bike() {
    }

    public int getId_bike() {
        return id_bike;
    }

    public void setId_bike(int id_bike) {
        this.id_bike = id_bike;
    }

    public String getBikeType() {
        return bikeType;
    }

    public void setBikeType(String bikeType) {
        this.bikeType = bikeType;
    }

    public int getEngine() {
        return engine;
    }

    public void setEngine(int engine) {
        this.engine = engine;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }
}
