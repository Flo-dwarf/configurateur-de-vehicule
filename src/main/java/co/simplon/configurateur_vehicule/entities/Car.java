package co.simplon.configurateur_vehicule.entities;

public class Car {
    private int id_car;
    private String carType;
    private int numberOfDoor;
    private boolean gearBox;
    private int horsePower;

    public Car(int id_car, String carType, int numberOfDoor, boolean gearBox, int horsePower) {
        this.id_car = id_car;
        this.carType = carType;
        this.numberOfDoor = numberOfDoor;
        this.gearBox = gearBox;
        this.horsePower = horsePower;
    }

    public Car() {
    }

    public int getId_car() {
        return id_car;
    }

    public void setId_car(int id_car) {
        this.id_car = id_car;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public int getNumberOfDoor() {
        return numberOfDoor;
    }

    public void setNumberOfDoor(int numberOfDoor) {
        this.numberOfDoor = numberOfDoor;
    }

    public boolean isGearBox() {
        return gearBox;
    }

    public void setGearBox(boolean gearBox) {
        this.gearBox = gearBox;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }
}
