package co.simplon.configurateur_vehicule.entities;

import java.util.Date;

public class MotorVehicle extends Vehicle {
    private int mileage;
    private Date dateOfTheFirstCirculation;

    public MotorVehicle(int mileage, Date dateOfTheFirstCirculation) {
        this.mileage = mileage;
        this.dateOfTheFirstCirculation = dateOfTheFirstCirculation;
    }

    public MotorVehicle() {
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public Date getDateOfTheFirstCirculation() {
        return dateOfTheFirstCirculation;
    }

    public void setDateOfTheFirstCirculation(Date dateOfTheFirstCirculation) {
        this.dateOfTheFirstCirculation = dateOfTheFirstCirculation;
    }
}
