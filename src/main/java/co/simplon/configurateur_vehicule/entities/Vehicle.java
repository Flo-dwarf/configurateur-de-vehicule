package co.simplon.configurateur_vehicule.entities;

import java.util.Date;

public class Vehicle {
    private String brand;
    private String color;
    private Date dateOfPurchase;
    private double purchasePrice;
    private double sellingPrice;

    public Vehicle(String brand, String color, Date dateOfPurchase, double purchasePrice, double sellingPrice) {
        this.brand = brand;
        this.color = color;
        this.dateOfPurchase = dateOfPurchase;
        this.purchasePrice = purchasePrice;
        this.sellingPrice = sellingPrice;
    }

    public Vehicle() {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(Date dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public double getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(double sellingPrice) {
        this.sellingPrice = sellingPrice;
    }
}
