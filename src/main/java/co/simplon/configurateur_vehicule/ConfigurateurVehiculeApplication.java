package co.simplon.configurateur_vehicule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigurateurVehiculeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigurateurVehiculeApplication.class, args);
	}

}
